function d(n, t) {
    var r = (65535 & n) + (65535 & t);
    return (n >> 16) + (t >> 16) + (r >> 16) << 16 | 65535 & r
}
function f(n, t, r, e, o, u) {
    return d((u = d(d(t, n), d(e, u))) << o | u >>> 32 - o, r)
}
function l(n, t, r, e, o, u, c) {
    return f(t & r | ~t & e, n, t, o, u, c)
}
function g(n, t, r, e, o, u, c) {
    return f(t & e | r & ~e, n, t, o, u, c)
}
function v(n, t, r, e, o, u, c) {
    return f(t ^ r ^ e, n, t, o, u, c)
}
function m(n, t, r, e, o, u, c) {
    return f(r ^ (t | ~e), n, t, o, u, c)
}
function c(n, t) {
    var r, e, o, u;
    n[t >> 5] |= 128 << t % 32,
    n[14 + (t + 64 >>> 9 << 4)] = t;
    for (var c = 1732584193, f = -271733879, i = -1732584194, a = 271733878, h = 0; h < n.length; h += 16)
        c = l(r = c, e = f, o = i, u = a, n[h], 7, -680876936),
        a = l(a, c, f, i, n[h + 1], 12, -389564586),
        i = l(i, a, c, f, n[h + 2], 17, 606105819),
        f = l(f, i, a, c, n[h + 3], 22, -1044525330),
        c = l(c, f, i, a, n[h + 4], 7, -176418897),
        a = l(a, c, f, i, n[h + 5], 12, 1200080426),
        i = l(i, a, c, f, n[h + 6], 17, -1473231341),
        f = l(f, i, a, c, n[h + 7], 22, -45705983),
        c = l(c, f, i, a, n[h + 8], 7, 1770035416),
        a = l(a, c, f, i, n[h + 9], 12, -1958414417),
        i = l(i, a, c, f, n[h + 10], 17, -42063),
        f = l(f, i, a, c, n[h + 11], 22, -1990404162),
        c = l(c, f, i, a, n[h + 12], 7, 1804603682),
        a = l(a, c, f, i, n[h + 13], 12, -40341101),
        i = l(i, a, c, f, n[h + 14], 17, -1502002290),
        c = g(c, f = l(f, i, a, c, n[h + 15], 22, 1236535329), i, a, n[h + 1], 5, -165796510),
        a = g(a, c, f, i, n[h + 6], 9, -1069501632),
        i = g(i, a, c, f, n[h + 11], 14, 643717713),
        f = g(f, i, a, c, n[h], 20, -373897302),
        c = g(c, f, i, a, n[h + 5], 5, -701558691),
        a = g(a, c, f, i, n[h + 10], 9, 38016083),
        i = g(i, a, c, f, n[h + 15], 14, -660478335),
        f = g(f, i, a, c, n[h + 4], 20, -405537848),
        c = g(c, f, i, a, n[h + 9], 5, 568446438),
        a = g(a, c, f, i, n[h + 14], 9, -1019803690),
        i = g(i, a, c, f, n[h + 3], 14, -187363961),
        f = g(f, i, a, c, n[h + 8], 20, 1163531501),
        c = g(c, f, i, a, n[h + 13], 5, -1444681467),
        a = g(a, c, f, i, n[h + 2], 9, -51403784),
        i = g(i, a, c, f, n[h + 7], 14, 1735328473),
        c = v(c, f = g(f, i, a, c, n[h + 12], 20, -1926607734), i, a, n[h + 5], 4, -378558),
        a = v(a, c, f, i, n[h + 8], 11, -2022574463),
        i = v(i, a, c, f, n[h + 11], 16, 1839030562),
        f = v(f, i, a, c, n[h + 14], 23, -35309556),
        c = v(c, f, i, a, n[h + 1], 4, -1530992060),
        a = v(a, c, f, i, n[h + 4], 11, 1272893353),
        i = v(i, a, c, f, n[h + 7], 16, -155497632),
        f = v(f, i, a, c, n[h + 10], 23, -1094730640),
        c = v(c, f, i, a, n[h + 13], 4, 681279174),
        a = v(a, c, f, i, n[h], 11, -358537222),
        i = v(i, a, c, f, n[h + 3], 16, -722521979),
        f = v(f, i, a, c, n[h + 6], 23, 76029189),
        c = v(c, f, i, a, n[h + 9], 4, -640364487),
        a = v(a, c, f, i, n[h + 12], 11, -421815835),
        i = v(i, a, c, f, n[h + 15], 16, 530742520),
        c = m(c, f = v(f, i, a, c, n[h + 2], 23, -995338651), i, a, n[h], 6, -198630844),
        a = m(a, c, f, i, n[h + 7], 10, 1126891415),
        i = m(i, a, c, f, n[h + 14], 15, -1416354905),
        f = m(f, i, a, c, n[h + 5], 21, -57434055),
        c = m(c, f, i, a, n[h + 12], 6, 1700485571),
        a = m(a, c, f, i, n[h + 3], 10, -1894986606),
        i = m(i, a, c, f, n[h + 10], 15, -1051523),
        f = m(f, i, a, c, n[h + 1], 21, -2054922799),
        c = m(c, f, i, a, n[h + 8], 6, 1873313359),
        a = m(a, c, f, i, n[h + 15], 10, -30611744),
        i = m(i, a, c, f, n[h + 6], 15, -1560198380),
        f = m(f, i, a, c, n[h + 13], 21, 1309151649),
        c = m(c, f, i, a, n[h + 4], 6, -145523070),
        a = m(a, c, f, i, n[h + 11], 10, -1120210379),
        i = m(i, a, c, f, n[h + 2], 15, 718787259),
        f = m(f, i, a, c, n[h + 9], 21, -343485551),
        c = d(c, r),
        f = d(f, e),
        i = d(i, o),
        a = d(a, u);
    return [c, f, i, a]
}
function i(n) {
    for (var t = "", r = 32 * n.length, e = 0; e < r; e += 8)
        t += String.fromCharCode(n[e >> 5] >>> e % 32 & 255);
    return t
}
function a(n) {
    var t = [];
    for (t[(n.length >> 2) - 1] = void 0,
    e = 0; e < t.length; e += 1)
        t[e] = 0;
    for (var r = 8 * n.length, e = 0; e < r; e += 8)
        t[e >> 5] |= (255 & n.charCodeAt(e / 8)) << e % 32;
    return t
}
function e(n) {
    for (var t, r = "0123456789abcdef", e = "", o = 0; o < n.length; o += 1)
        t = n.charCodeAt(o),
        e += r.charAt(t >>> 4 & 15) + r.charAt(15 & t);
    return e
}
function r(n) {
    return unescape(encodeURIComponent(n))
}
function o(n) {
    return i(c(a(n = r(n)), 8 * n.length))
}
function u(n, t) {
    return function(n, t) {
        var r, e = a(n), o = [], u = [];
        for (o[15] = u[15] = void 0,
        16 < e.length && (e = c(e, 8 * n.length)),
        r = 0; r < 16; r += 1)
            o[r] = 909522486 ^ e[r],
            u[r] = 1549556828 ^ e[r];
        return t = c(o.concat(a(t)), 512 + 8 * t.length),
        i(c(u.concat(t), 640))
    }(r(n), r(t))
}
function t(n, t, r) {
    return t ? r ? u(t, n) : e(u(t, n)) : r ? o(n) : e(o(n))
}
md5=t;
function customStrDecode(_0xd23398) {
    key = md5('test'),
    _0xd23398 = window.atob(_0xd23398),
    len = key.length,
    code = '';
    for (i = 0x0; i<_0xd23398.length; i++) {
        k = i%len,
        code += String.fromCharCode(_0xd23398.charCodeAt(i)^key.charCodeAt(k));
    }
    return window.atob(code);
}
function indexOfVal(_0x51038c, _0x3d2780) {
    var _0xb316e2 = {
        'VCLqf': function(_0x2343cf, _0x3b754f) {
            return _0x2343cf < _0x3b754f;
        }
    };
    for (var _0x3f75f3 = 0x0; _0xb316e2['VCLqf'](_0x3f75f3, _0x51038c['length']); _0x3f75f3++) {
        if (_0x3d2780 === _0x51038c[_0x3f75f3])
            return !![];
    }
    return ![];
}
function deString(_0x337782, _0x45b5d6, _0x589c2d) {
    var _0xb4bd6f = {
        'KtTuh': function(_0x465dc6, _0x272eec) {
            return _0x465dc6 < _0x272eec;
        },
        'gNOIr': function(_0x23c921, _0x1fa570) {
            return _0x23c921 > _0x1fa570;
        },
        'Gxrex': function(_0x350cee, _0x3eb3a9) {
            return _0x350cee < _0x3eb3a9;
        },
        'KKZor': function(_0xc9ad1a, _0x3f0395) {
            return _0xc9ad1a | _0x3f0395;
        },
        'vWcAW': function(_0x3027cf, _0x4867ee) {
            return _0x3027cf >> _0x4867ee;
        },
        'TENeX': function(_0x580fac, _0x598a4a) {
            return _0x580fac | _0x598a4a;
        },
        'MyKKv': function(_0x4784be, _0x15d318) {
            return _0x4784be & _0x15d318;
        },
        'LynQc': function(_0x303b9c, _0x287ef3) {
            return _0x303b9c < _0x287ef3;
        },
        'oiEWy': function(_0x50e283, _0x5b931e) {
            return _0x50e283 === _0x5b931e;
        },
        'ByIyB': 'iBmUb',
        'Auxlw': 'ScVeq'
    }
      , _0x45ce26 = ''
      , _0x311a7f = _0x337782
      , _0x836c4e = _0x45b5d6
      , _0x302df4 = _0x589c2d.split('');
    for (var _0x6af1ec = 0x0; _0x6af1ec < _0x302df4['length']; _0x6af1ec++) {
        if (_0xb4bd6f['oiEWy']('iBmUb', _0xb4bd6f['ByIyB'])) {
            var _0x7a1451 = _0x302df4[_0x6af1ec]
              , _0x355a55 = /^[a-zA-Z]+$/['test'](_0x7a1451);
            if (_0x355a55 && indexOfVal(_0x836c4e, _0x7a1451))
                _0x45ce26 += _0x836c4e[_0x311a7f['indexOf'](_0x7a1451)];
            else {
                if (_0xb4bd6f['oiEWy'](_0xb4bd6f['Auxlw'], _0xb4bd6f['Auxlw']))
                    _0x45ce26 += _0x7a1451;
                else {
                    var _0x13c200 = _0x19cbc6['charCodeAt'](_0x43428e);
                    if (_0xb4bd6f['KtTuh'](_0x13c200, 0x80))
                        _0x59778a += _0x687f74['fromCharCode'](_0x13c200);
                    else
                        _0xb4bd6f['gNOIr'](_0x13c200, 0x7f) && _0xb4bd6f['Gxrex'](_0x13c200, 0x800) ? (_0x51773c += _0x3ec764['fromCharCode'](_0xb4bd6f['KKZor'](_0xb4bd6f['vWcAW'](_0x13c200, 0x6), 0xc0)),
                        _0xd6ab52 += _0xf7079b['fromCharCode'](_0xb4bd6f['KKZor'](_0x13c200 & 0x3f, 0x80))) : (_0x85b5f7 += _0x94d5['fromCharCode'](_0xb4bd6f['vWcAW'](_0x13c200, 0xc) | 0xe0),
                        _0x57a064 += _0x9ba6bd['fromCharCode'](_0xb4bd6f['KKZor'](_0x13c200 >> 0x6 & 0x3f, 0x80)),
                        _0x25d73b += _0x1bf402['fromCharCode'](_0xb4bd6f['TENeX'](_0xb4bd6f['MyKKv'](_0x13c200, 0x3f), 0x80)));
                }
            }
        } else {
            for (var _0x2e2351 = 0x0; _0xb4bd6f['LynQc'](_0x2e2351, _0x5cec1a['length']); _0x2e2351++) {
                if (_0x3bcb71 === _0x26690a[_0x2e2351])
                    return !![];
            }
            return ![];
        }
    }
    return _0x45ce26;
}
function sign(_0x278289) {
    _0x278289 = customStrDecode(_0x278289)
      , _0x45f1fe = _0x278289.split('/')
      , _0x400167 = '';
    for (var _0x317232 = 0x0; _0x317232 < _0x45f1fe.length; _0x317232++) {
        var _0xb57ed3 = (_0x317232+ 0x1) == _0x45f1fe.length ? '' : '/';
        if ((_0x317232 == 0x0) || (_0x317232 == 0x1)) {} else {
            if ('bwbpU' != 'LRnAo')
                _0x400167 += (_0x45f1fe[_0x317232] + _0xb57ed3);
            else
                return !![];
        }
    }
    var _0x413ec6 = window.atob(_0x400167)
      , _0xb5279f = deString(JSON['parse'](window.atob(_0x45f1fe[0x1])), JSON['parse'](window.atob(_0x45f1fe[0x0])), _0x413ec6);
    return _0xb5279f;
}
sign("ZgpULXsJDxRQBF5aNQNbSTA2UQtVMEB3YVtafQNsAkZnVXYsVVIPEHtxXnw1A0dGMDYIVlUzVAZhbnx9AFkoRmR8di1XNw8UUAReWwVmW0kwNlVWVTBAd2FYcH0DbAJGaW5ULFVSDxFiYV58NQNYAjA2CFZVMn53YW58fQBfMEZkfHYtV1IPFFAEXlAFZltJMDZRE1UwQHdhWHh9A2wCRmlUVCxVUg8RZWFefDUDRwUwNghWVTJyZ2FbY0E0Bwp9VVJUFlJQDw9RY15LN2QGdQI0HCFnCEB5U25WRztjCnxTCVQSYTUPKWUGQgU3ZFsAAjYuIWc9dnlQY15HNnEofX1SVBZSUA8OVWNeSzdkBn0CNBwhZwtqeVNuVkc7Yih8UwlUElVQDyllBkJEN2RbAAI2NiFnPXZ5UGJgRzZxKH19bFQWUlAPCGUGXks3ZAJFAjQcIWcIAXlTblZHO1oKfFMJVBNXUA8pZQZCAjdkWwACNxQhZwhpRWhzWlwvBjBvYW1iK1M2DxBtXHx+AHRlawAmVShjIHJAaGNWQC8FFgN+YVwMZScTHm5wUQUAZH17NAwQKHkIUABrXWB9Blk3QVN8ASlUN1YUYV1kQjUDBmY0DzIwZlVhS2RcdEUvBTBzZFQJDmQnOT5tXHxyMQJTey42IihiPVwDa11kBwZjN05TfFQ8VCoiVmNaVmI3Xnl0MA0qBFYOcl5rWlZYAHIsQmJUfg17JCoTYFtKZgUCbXcuNSYOeTBiZmhycE0vWCtPZ1VqB2AJUldhYQNGB3YCQS03LgxRCXJGZHBWAwZyLHBlYUgzVCQpIGBaaGYAXmV0MA0AKlYhVGtnXVpkLwRTc2JXejF7UDFUYHBgWjJlU0IwDSIOUQltA2hyewcvYgpjZG1+DmQ0KQVgXHBGN2V9dDkpFDdVDmECZ11WWC9fUk9+bHY2ZDQTPm5jYGMpAm1lASQuMXkyQH5lXWcHB2MwQlNxXD5gNiUMYmFGXgBYAmsCJSo1Zg4NB2ZbB1gwWC9OYlVuDGQJOh1hYnxfAXVtZDUPCChWM34Fa25CWS4GMG5RcX4HUwglJ2FgaFoyZX1rOSQ+DmIfVH5oYUJcA3MgAGlDcgpgOjEhYVpoUDFkeQUCJxQOViBIeWh+QnAvBTx\/Z1VuMWQ3Vj5hBmBaKXRhRy40NgR5IG1Ea1sCTwYFFmF\/bgENVA4TVWBgZAUHAlNCMgxdN2IfVFJnYQJPA2MgY2Nteg9gNCkdYWBWRjB1bUIBJD4PYg5IRWdaZwI2WAphan5QDmA6CxBuYUZ3AV95ay40CDJ5CEB\/aHJ8ZDZZIH9jbX4\/ewg1MWJdWmYxX1N1NgsiKmYzYkFrW0ZELGIWcGJScgpUJQQeYQVCcgB1U2c0Dz4EYlRyS2dwZHwzBSBiZ1VcDmRRIRFhcGBfKQMCQjkbKipVCFtJZltCWS8EU0dqQ3Fb");


//@js: jsHtml=java.post(java.get('homeUrl')+'/player/api.php','vid='+java.get('playerUrl'),{referer:String(java.get('homeUrl'))}).body();url=JSON.parse(jsHtml).data.url; function customStrDecode(_0xd23398){key=String(java.md5Encode('test')),_0xd23398=String(java.base64Decode(_0xd23398)),len=key.length,code='';for(i=0x0;i<_0xd23398.length;i++){k=i%len,code+=String.fromCharCode(_0xd23398.charCodeAt(i)^key.charCodeAt(k));} return String(java.base64Decode(code));} function indexOfVal(_0x51038c,_0x3d2780){var _0xb316e2={'VCLqf':function(_0x2343cf,_0x3b754f){return _0x2343cf<_0x3b754f;}};for(var _0x3f75f3=0x0;_0xb316e2['VCLqf'](_0x3f75f3,_0x51038c['length']);_0x3f75f3++){if(_0x3d2780===_0x51038c[_0x3f75f3]) return!![];} return![];} function deString(_0x337782,_0x45b5d6,_0x589c2d){var _0xb4bd6f={'KtTuh':function(_0x465dc6,_0x272eec){return _0x465dc6<_0x272eec;},'gNOIr':function(_0x23c921,_0x1fa570){return _0x23c921>_0x1fa570;},'Gxrex':function(_0x350cee,_0x3eb3a9){return _0x350cee<_0x3eb3a9;},'KKZor':function(_0xc9ad1a,_0x3f0395){return _0xc9ad1a|_0x3f0395;},'vWcAW':function(_0x3027cf,_0x4867ee){return _0x3027cf>>_0x4867ee;},'TENeX':function(_0x580fac,_0x598a4a){return _0x580fac|_0x598a4a;},'MyKKv':function(_0x4784be,_0x15d318){return _0x4784be&_0x15d318;},'LynQc':function(_0x303b9c,_0x287ef3){return _0x303b9c<_0x287ef3;},'oiEWy':function(_0x50e283,_0x5b931e){return _0x50e283===_0x5b931e;},'ByIyB':'iBmUb','Auxlw':'ScVeq'},_0x45ce26='',_0x311a7f=_0x337782,_0x836c4e=_0x45b5d6,_0x302df4=_0x589c2d.split('');for(var _0x6af1ec=0x0;_0x6af1ec<_0x302df4['length'];_0x6af1ec++){if(_0xb4bd6f['oiEWy']('iBmUb',_0xb4bd6f['ByIyB'])){var _0x7a1451=_0x302df4[_0x6af1ec],_0x355a55=/^[a-zA-Z]+$/['test'](_0x7a1451);if(_0x355a55&&indexOfVal(_0x836c4e,_0x7a1451)) _0x45ce26+=_0x836c4e[_0x311a7f['indexOf'](_0x7a1451)];else{if(_0xb4bd6f['oiEWy'](_0xb4bd6f['Auxlw'],_0xb4bd6f['Auxlw'])) _0x45ce26+=_0x7a1451;else{var _0x13c200=_0x19cbc6['charCodeAt'](_0x43428e);if(_0xb4bd6f['KtTuh'](_0x13c200,0x80)) _0x59778a+=_0x687f74['fromCharCode'](_0x13c200);else _0xb4bd6f['gNOIr'](_0x13c200,0x7f)&&_0xb4bd6f['Gxrex'](_0x13c200,0x800)?(_0x51773c+=_0x3ec764['fromCharCode'](_0xb4bd6f['KKZor'](_0xb4bd6f['vWcAW'](_0x13c200,0x6),0xc0)),_0xd6ab52+=_0xf7079b['fromCharCode'](_0xb4bd6f['KKZor'](_0x13c200&0x3f,0x80))):(_0x85b5f7+=_0x94d5['fromCharCode'](_0xb4bd6f['vWcAW'](_0x13c200,0xc)|0xe0),_0x57a064+=_0x9ba6bd['fromCharCode'](_0xb4bd6f['KKZor'](_0x13c200>>0x6&0x3f,0x80)),_0x25d73b+=_0x1bf402['fromCharCode'](_0xb4bd6f['TENeX'](_0xb4bd6f['MyKKv'](_0x13c200,0x3f),0x80)));}}}else{for(var _0x2e2351=0x0;_0xb4bd6f['LynQc'](_0x2e2351,_0x5cec1a['length']);_0x2e2351++){if(_0x3bcb71===_0x26690a[_0x2e2351]) return!![];} return![];}} return _0x45ce26;} function sign(_0x278289){_0x278289=customStrDecode(_0x278289),_0x45f1fe=_0x278289.split('/'),_0x400167='';for(var _0x317232=0x0;_0x317232<_0x45f1fe.length;_0x317232++){var _0xb57ed3=(_0x317232+0x1)==_0x45f1fe.length?'':'/';if((_0x317232==0x0)||(_0x317232==0x1)){}else{if('bwbpU'!='LRnAo') _0x400167+=(_0x45f1fe[_0x317232]+_0xb57ed3);else return!![];}} var _0x413ec6=String(java.base64Decode(_0x400167)),_0xb5279f=deString(JSON['parse'](String(java.base64Decode(_0x45f1fe[0x1]))),JSON['parse'](String(java.base64Decode(_0x45f1fe[0x0]))),_0x413ec6);return _0xb5279f;};sign(url)