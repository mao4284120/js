# -*- coding: utf-8 -*-
import fruit_pb2

UserData = fruit_pb2.apple()


UserData.color =  16777215
UserData.content= "热乎的"
UserData.ctime=1657883124
UserData.fontsize= 25
UserData.id= 1097162187744614400
UserData.idStr= "1097162187744614400"
UserData.midHash= "69b24fe7"
UserData.mode= 1
UserData.progress= 5813
UserData.weight= 1

print("=====================原始数据=======================")
print(UserData)
# 序列化
UserData1 = UserData.SerializeToString()
print("====================序列化数据=======================")
print(UserData1)

# 反序列化
UserData2 = fruit_pb2.apple()
UserData2.ParseFromString(UserData1)
print("\r\n===================反序列化数据======================")
print(UserData2)
