import json
import requests

def addOrder():
    books = requests.get("http://192.168.190.76:1130/getBookSources").json().get("data")
    #print(books)
    customOrder = 2590
    i = 0
    j = 200
    k = 2000
    l = 3000
    m = 8000
    n = 9000
    o = 4000
    newBooks = []
    for book in books:
        if book.get("bookSourceGroup","").find("🌙 ΑΡΙ") > -1:
            book["customOrder"] = i
            i = i + 1
        elif book.get("bookSourceGroup","").find("☸") > -1:
            book["enabledCookieJar"] = True
            book["customOrder"] = k
            book["header"]="<js>\nheader={\"User-Agent\":\"Mozilla/5.0 (Linux; Android 8.1.0; JKM-AL00b Build/HUAWEIJKM-AL00b; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/66.0.3359.126 MQQBrowser/6.2 TBS/044807 Mobile Safari/537.36\",\"X-Requested-With\":\"XMLHttpRequest\"};\nJSON.stringify(header);\n</js>"
            if book.get("searchUrl","").find("legadoPost") == -1 and book.get("searchUrl","").find("POST") > -1:
                book["searchUrl"] =  "<js>\nlegadoPost(/<form.*?action=\"([^\"g]+)\"[\\s\\S]*?form>/,{source:source});\n</js>" 
            elif book.get("searchUrl","").find("legadoGet") == -1 and book.get("searchUrl","").find("POST") == -1:
                book["searchUrl"] =  "<js>\nlegadoGet(/<form.*?action=\"([^\"g]+)\"[\\s\\S]*?form>/,{source:source});\n</js>" 
            if book.get("ruleSearch").get("bookList").find("validateCode") == -1:
                book["ruleSearch"]["bookList"]="<js>validateCode(result);</js>\n"+book["ruleSearch"]["bookList"]
            k = k + 1
        elif book.get("bookSourceGroup","").find("无搜索") > -1:
            book["customOrder"] = l
            l = l + 1
        elif book.get("bookSourceGroup","").find("验证码") > -1:
            book["enabledCookieJar"] = True
            if book.get("ruleSearch").get("bookList").find("validateCode") == -1 and book.get("ruleSearch").get("bookList").find("<js") == -1:
                book["ruleSearch"]["bookList"]="<js>validateCode(result);</js>\n"+book["ruleSearch"]["bookList"]
            if book.get("searchUrl","").find("code=0000") > -1:
                book["searchUrl"] =  "<js>\nbody=\"code=0000\";\nurl=legadoPost(/<form.*?action=\"([^\"g]+)\"[\\s\\S]*?form>/,{body:body,source:source});\n</js>"
            if book.get("searchUrl","").find("captcha=0000") > -1:
                book["searchUrl"] =  "<js>\nbody=\"captcha=0000\";\nurl=legadoPost(/<form.*?action=\"([^\"g]+)\"[\\s\\S]*?form>/,{body:body,source:source});\n</js>"
            if book.get("searchUrl","").find("checkcode=0000") > -1:
                book["searchUrl"] =  "<js>\nbody=\"checkcode=0000\";\nurl=legadoPost(/<form.*?action=\"([^\"g]+)\"[\\s\\S]*?form>/,{body:body,source:source});\n</js>"                                        
            book["customOrder"] = o
            o = o + 1    
        elif book.get("bookSourceGroup","").find("✈️") > -1:
            book["customOrder"] = m
            m = m + 1    
        elif book.get("bookSourceGroup","").find("模板") > -1:
            book["customOrder"] = n
            n = n + 1
        else:
            book["customOrder"] = j
            j = j + 1
        #if book.get("exploreUrl","").find("source.getKey()") > -1:
            #book["exploreUrl"] = book.get("exploreUrl","").replace("url=source.getKey()","let url=source.getKey()")
            #book["exploreUrl"] = book.get("exploreUrl","").replace("source.getKey()","\""+book.get("bookSourceUrl","")+"\"")
            #book["exploreUrl"] = book.get("exploreUrl","").replace("purl=String(url);","let purl=String(url);")
            #book["exploreUrl"] = book.get("exploreUrl","").replace("list=buildA(","let list=buildA(")
            #book["exploreUrl"] = book.get("exploreUrl","").replace("list=build(","let list=build(")
        #if book.get("searchUrl","").find("extra={source:source}") > -1:
        #    book["searchUrl"] = book.get("searchUrl","").replace("extra={source:source}","extra={source:source,url:\""+book.get("bookSourceUrl","")+"\"}")
        #    book["searchUrl"] = book.get("searchUrl","").replace("source.getKey()","\""+book.get("bookSourceUrl","")+"\"")
        #if book.get("searchUrl","").find("buildMapUrl(key)") > -1:
        #    book["searchUrl"] = book.get("searchUrl","").replace("buildMapUrl(key)","extra={source:source,url:\""+book.get("bookSourceUrl","")+"\"};\nbuildMapUrl(key,extra);")
        #    book["searchUrl"] = book.get("searchUrl","").replace("source.getKey()","\""+book.get("bookSourceUrl","")+"\"")
        newBooks.append(book)
    with open(f'bookSource.json','w',encoding='utf-8') as f:
        json.dump(newBooks, f, ensure_ascii=False,sort_keys=True, indent=4, separators=(',', ':')) 

if __name__ == '__main__': 
    addOrder()