function legadoProcessor(process) {
    java = process.java;
    source = process.source;
    cookie = process.cookie;
    cache = process.cache;
    this.key = process.key;
    this.page = process.page;
    this.baseUrl = process.baseUrl;
    this.process = process;
    this.params = {};
    this.params.post = "POST";
    this.params.get = "GET";
    this.params.postfix_selector = ":not([href~=javascript]):not([href=/]):not([href^=#]):not(:matches(记录|首\\s*页|搜索|足迹|登|注册|轨迹|^\\s*$))";
    this.params.sort_selector = "a:matches((书\\s*库|分\\s*类|玄\\s*幻|频|连))" + this.params.postfix_selector;
    this.params.top_selector = "a:matches((排\\s*行|热\\s*门|榜|最\\s*近|在\\s*线|人\\s*气)):not(:contains(桌面))" + this.params.postfix_selector;
    this.params.quanben_selector = "a:matches((全\\s*本|完\\s*本|完\\s*结))" + this.params.postfix_selector;
    this.validateRegexMap = [
        {
            reg: /no-js|Just a moment|人机验证|阅读云/,
            exec: () => {
                x = java.get("x");
                java.log(x);
                t = java.startBrowserAwait(x, "验证");
                url = source.getKey();
                x = x.match(/([^,]+),({.*})/);
                java.log(JSON.stringify(x));
                ck = cookie.getCookie(x[1]);
                java.log(ck);
                post = JSON.parse(x[2]);
                headers = post["headers"] || {};
                headers["Cookie"] = ck;
                post["headers"] = headers;
                java.put(java.md5Encode16(url + "cookie"), ck);
                html = java.ajax(x[1] + "," + JSON.stringify(post));
                return html;
            },
        },
        {
            reg: /styles\/code.php/,
            exec: () => {
                x = java.get("x");
                java.log(x);
                code = java.getVerificationCode(source.getKey() + "/styles/code.php");
                x = x.replace("code=0000", "code=" + code);
                url = source.getKey();
                x = x.match(/([^,]+),({.*})/);
                java.log(JSON.stringify(x));
                ck = cookie.getCookie(x[1]);
                java.log(ck);
                post = JSON.parse(x[2]);
                headers = post["headers"] || {};
                headers["Cookie"] = ck;
                post["headers"] = headers;
                java.put(java.md5Encode16(url + "cookie"), ck);
                html = java.ajax(x[1] + "," + JSON.stringify(post));
                return html;
            },
        },
        {
            reg: /url: "\/code",/,
            exec: () => {
                x = java.get("x");
                java.log(x);
                data = JSON.parse(java.post(source.getKey() + "/code", "", {}).body());
                //java.log(JSON.stringify(data));
                code = java.getVerificationCode(data.Image);
                id = data.CId;
                x = x.replace("captcha=0000", "code=" + code + "&id=" + id);
                url = source.getKey();
                x = x.match(/([^,]+),({.*})/);
                java.log(JSON.stringify(x));
                ck = cookie.getCookie(x[1]);
                java.log(ck);
                post = JSON.parse(x[2]);
                headers = post["headers"] || {};
                headers["Cookie"] = ck;
                post["headers"] = headers;
                java.put(java.md5Encode16(url + "cookie"), ck);
                html = java.ajax(x[1] + "," + JSON.stringify(post));
                return html;
            }
        },
        {
            reg: /captcha.php/,
            exec: () => {
                x = java.get("x");
                java.log(x);
                code = java.getVerificationCode(source.getKey() + "/captcha.php");
                x = x.replace("captcha=0000", "captcha=" + code);
                url = source.getKey();
                x = x.match(/([^,]+),({.*})/);
                java.log(JSON.stringify(x));
                ck = cookie.getCookie(x[1]);
                java.log(ck);
                post = JSON.parse(x[2]);
                headers = post["headers"] || {};
                headers["Cookie"] = ck;
                post["headers"] = headers;
                java.put(java.md5Encode16(url + "cookie"), ck);
                html = java.ajax(x[1] + "," + JSON.stringify(post));
                return html;
            },
        },
        {
            reg: /checkcode.php/,
            exec: () => {
                x = java.get("x");
                java.log(x);
                code = java.getVerificationCode(source.getKey() + "/checkcode.php");
                x = x.replace("checkcode=0000", "checkcode=" + code);
                url = source.getKey();
                x = x.match(/([^,]+),({.*})/);
                java.log(JSON.stringify(x));
                ck = cookie.getCookie(x[1]);
                java.log(ck);
                post = JSON.parse(x[2]);
                headers = post["headers"] || {};
                headers["Cookie"] = ck;
                post["headers"] = headers;
                java.put(java.md5Encode16(url + "cookie"), ck);
                html = java.ajax(x[1] + "," + JSON.stringify(post));
                return html;
            },
        },
        {
            reg: /api\/weixin.php/,
            exec: () => {
                x = java.get("x");
                java.log(x);
                code = java.getVerificationCode(source.getKey() + "/api/weixin.php?action=wx_verity_code");
                x = x.replace("wx_verity_code=0000", "wx_verity_code=" + code);
                url = source.getKey();
                x = x.match(/([^,]+),({.*})/);
                java.log(JSON.stringify(x));
                ck = cookie.getCookie(x[1]);
                java.log(ck);
                post = JSON.parse(x[2]);
                headers = post["headers"] || {};
                headers["Cookie"] = ck;
                post["headers"] = headers;
                java.put(java.md5Encode16(url + "cookie"), ck);
                html = java.ajax(x[1] + "," + JSON.stringify(post));
                return html;
            },
        },
        {
            reg: /searchcode.php/,
            exec: () => {
                x = java.get("x");
                java.log(x);
                code = java.getVerificationCode(source.getKey() + "/searchcode.php");
                x = x.replace("code=0000", "code=" + code);
                url = source.getKey();
                x = x.match(/([^,]+),({.*})/);
                java.log(JSON.stringify(x));
                ck = cookie.getCookie(x[1]);
                java.log(ck);
                post = JSON.parse(x[2]);
                headers = post["headers"] || {};
                headers["Cookie"] = ck;
                post["headers"] = headers;
                java.put(java.md5Encode16(url + "cookie"), ck);
                html = java.ajax(x[1] + "," + JSON.stringify(post));
                return html;
            },
        },
        {
            reg: /css\/code.php/,
            exec: () => {
                x = java.get("x");
                java.log(x);
                code = java.getVerificationCode(source.getKey() + "/17mb/css/code.php");
                x = x.replace("code=0000", "code=" + code);
                url = source.getKey();
                x = x.match(/([^,]+),({.*})/);
                java.log(JSON.stringify(x));
                ck = cookie.getCookie(x[1]);
                java.log(ck);
                post = JSON.parse(x[2]);
                headers = post["headers"] || {};
                headers["Cookie"] = ck;
                post["headers"] = headers;
                java.put(java.md5Encode16(url + "cookie"), ck);
                html = java.ajax(x[1] + "," + JSON.stringify(post));
                return html;
            },
        },
        {
            reg: /安全验证/,
            exec: () => {
                x = java.get("x");
                java.log(x);
                key = java.get("key");
                body = "validate=0000&searchword={" + "{key}}";
                url = source.getKey() + "/search.php?scheckAC=check&page=&searchtype=&order=&tid=&area=&year=&letter=&yuyan=&state=&money=&ver=&jq=";
                x = this.request(this.params.post, /<form.*?action="([^"]+)"[\s\S]*?form>/, { body: body, source: source, save: false, url: url });
                code = java.getVerificationCode(source.getKey() + "/include/vdimgck.php");
                x = x.replace("validate=0000", "validate=" + code);
                url = source.getKey();
                x = x.match(/([^,]+),({.*})/);
                java.log(JSON.stringify(x));
                ck = cookie.getCookie(x[1]);
                java.log(ck);
                post = JSON.parse(x[2]);
                headers = post["headers"] || {};
                headers["Cookie"] = ck;
                post["headers"] = headers;
                java.put(java.md5Encode16(url + "cookie"), ck);
                html = java.ajax(x[1] + "," + JSON.stringify(post));
                return html;
            },
        },
        {
            reg: /内容正在载入/,
            exec: () => {
                c2 = html.match(/c2="(.*?)"/)[1]
                u2 = html.match(/src="(.*?)"/)[1]
                x = java.get("x");
                x = x.match(/([^,]+),({.*})/);
                baseUrl = x[1];
                origin = baseUrl.match(/[^.]+[^/]+/)[0]
                str = java.ajax(origin + u2).match(/function.*\(\)\}/)[0]
                eval(str); temp = ajax(c2)
                java.ajax(origin + temp)
                if (baseUrl.match(/sscc/)) {
                    x = java.get("x")
                } else { x = baseUrl }
                url = source.getKey();
                x = x.match(/([^,]+),({.*})/);
                java.log(JSON.stringify(x));
                ck = cookie.getCookie(x[1]);
                java.log(ck);
                post = JSON.parse(x[2]);
                headers = post["headers"] || {};
                headers["Cookie"] = ck;
                post["headers"] = headers;
                java.put(java.md5Encode16(url + "cookie"), ck);
                html = java.ajax(x[1] + "," + JSON.stringify(post));
                return html;
            },
        }
    ]
    this.regexMap = [
        {
            reg: /(\/\w+\/)([a-z0-9]+)-(\w+)(-\d+)(-1.html)$/,
            exec: (top_url) => { return String(top_url).match(/(\/\w+\/)([a-z0-9]+)(-\w+)(-\d+)(-1.html)$/); }
        },
        {
            reg: /(\/\w+\/)([a-z0-9]+)_(\w+)(_\d+)(_0_1.html)$/,
            exec: (top_url) => { return String(top_url).match(/(\/\w+\/)([a-z0-9]+)(\w+)(_\d+)(_0_1.html)$/); }
        },
        {
            reg: /(\/\w+\/)([a-z0-9\/]*)\/([a-z0-9]*)\/(1.html)$/,
            exec: (top_url) => { return String(top_url).match(/(\/\w+\/)([a-z0-9\/]*)(\/[a-z0-9]*)(\/1.html)$/); }
        },
        {
            reg: /(\/\w+\/)([a-z0-9]+)_(\w+)(_\d+)(_1.html)$/,
            exec: (top_url) => { return String(top_url).match(/(\/\w+\/)([a-z0-9]+)(\w+)(_\d+)(_1.html)$/); }
        },
        {
            reg: /(\/)([a-z0-9\/]*)\/(\d+)(\/)$/,
            exec: (top_url) => { return String(top_url).match(/(\/)([a-z0-9\/]*)(\/\d+)(\/)$/); }
        },
        {
            reg: /(\/[a-z0-9]+_)([a-z0-9]*)_(\w+)(_0_1.html)$/,
            exec: (top_url) => { return String(top_url).match(/(\/[a-z0-9]+_)([a-z0-9]+)(\w+)(_0_1.html)$/); }
        },
        {
            reg: /(\/)(\w+\/[a-z]*)(\d*)(.html)$/,
            exec: (top_url) => { return String(top_url).match(/(\/)(\w+\/[a-z]*)(\d*)(.html)$/); }
        },
        {
            reg: /(\/modules\/article\/[a-z0-9]*.\w+\?)(page=\d+)*(&*sortid=\d+)*(&*order=\w+)*/,
            exec: (top_url) => { return String(top_url).match(/(\/modules\/article\/[a-z0-9]*.\w+\?)(page=\d+)*(&*sortid=\d+)*(&*order=\w+)*/); }
        },
        {
            reg: /(\/\w+\/)([a-z0-9]+)([a-z0-9-]+)(-\d+)(-0-1.html)$/,
            exec: (top_url) => { return String(top_url).match(/(\/\w+\/)([a-z0-9]+)([a-z0-9-]+)(-\d+)(-0-1.html)$/); }
        }
    ];
}

Object.assign(legadoProcessor.prototype, {
    constructor: legadoProcessor(this),
    combins(arguments) {
        if (arguments.length < 2 && !Array.isArray(arguments[0][0]))
            return arguments[0] || [];
        var args = arguments.length == 1 ? Array.prototype.slice.call(arguments[0]) : Array.prototype.slice.call(arguments);
        var that = {
            index: 0,
            nth: function (n) {
                var result = [],
                    d = 0;
                for (; d < this.dim; d++) {
                    var l = this[d].length;
                    var i = n % l;
                    result.push(this[d][i]);
                    n -= i;
                    n /= l;
                }
                return result;
            },
            next: function () {
                if (this.index >= size)
                    return;
                var result = this.nth(this.index);
                this.index++;
                return result;
            }
        };
        var size = 1;
        for (var i = 0; i < args.length; i++) {
            size = size * args[i].length;
            that[i] = args[i];
        }
        that.size = size;
        that.dim = args.length;
        return that;
    },
    createBody(forms, extra) {
        extra = extra || {};
        body = extra.body || "";
        inputs = [];
        search_url = "";
        if (forms) {
            forms.filter(form => {
                if (form.search("/login") > -1 || !/^<form/.test(form)) {
                    return false;
                }
                return true;
            }).map((form) => {
                search_url = form.match(/<form.*?action="([^"]+)"[\s\S]*?form>/);
                search_url = search_url ? search_url[1] : "";
                inputs = form.match(/<input.*?type="(text|search|hidden|key)".*?>/g);
                selects = form.match(/<select.*?name="([^"]+)"[\s\S]*?option.*?(?:selected="selected").*?value="([^"]+)"()?.*<\/select>/);
                java.log(String(inputs));
                java.log(String(selects));
                inputs = inputs.map(input => {
                    type = String(input).match(/type="([^"]+)"/);
                    title = String(input).match(/name="([^"]+)"/);
                    value = String(input).match(/value="([^"]+)"/);
                    title = title ? title[1] : "";
                    value = value ? value[1] : "";
                    type = type ? type[1] : "";
                    if (type === "text" || type === "search" || type === "key") {
                        value = "{" + "{key}}";
                    }
                    return [title, value].join("=");
                });
                if (selects) {
                    inputs.push(selects[1] + "=" + selects[2]);
                }
            });
            if (body != "")
                inputs.push(body);
            body = inputs.join("&");
            return [search_url, body];
        }
        return [search_url, body];
    },
    fixurl(oldurl, purl) {
        oldurl = /^https?/.test(url) ? String(oldurl).replace(/(^https?:)?\/\/.*?\//, "/") : oldurl;
        oldurl = /^\//.test(oldurl) ? oldurl : "/" + oldurl;
        oldurl = purl + String(oldurl);
        return oldurl;
    },
    request(method, regex, extra) {
        extra = extra || {};
        key = extra.key || this.key;
        page = extra.page || this.page;
        charset = extra.charset || "";
        save = typeof (extra.save) === 'undefined' ? true : extra.save;
        body = extra.body || "";
        url = extra.url || String(source.getKey()).match(/^(https|http|ftp)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z0-9]{2,}(:\d+)?/)[0]
        webview = typeof (extra.webview) === 'undefined' ? true : extra.webview;
        headers = extra.headers || {};
        java.put("key", key);
        java.put("page", page);
        time = this.Get("time") || new Date().getTime();
        if (this.Get("search_url") && save && new Date().getTime() - time < 580501424) {
            x = this.Get("search_url") || "";
            java.put("x", x.replace("{" + "{key}}", key));
            return x;
        } else {
            purl = String(source.getKey()).match(/^(https|http|ftp)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z0-9]{2,}(:\d+)?/)[0];
            html = webview ? java.ajax(url + ",{'webView': true}"):java.ajax(url);
            //java.log(html);
            forms = html.match(regex);
            java.log(String(forms));
            [form_url, body] = this.createBody(forms, { body: body });
            java.log(body);
            charsets = html.match(/meta.*charset="?([\w-]*)/);
            java.log(String(charsets));
            charset = charset || charsets ? charsets[1] : "" || "utf-8";
            if (new RegExp(purl.replace(/https?:/, "(^https?:)?") + "\/*$").test(url)) {
                prefix_url = this.fixurl(form_url, purl);
            } else {
                prefix_url = url;
            }
            post = {
                "charset": charset,
                "headers": headers
            };
            if (method == this.params.post) {
                post["body"] = body;
                post["method"] = this.params.post;
            } else if (method == this.params.get) {
                prefix_url = prefix_url + "?" + body;
            }
            if (save && body !== "") {
                java.log(prefix_url + "," + JSON.stringify(post));
                this.Put("time",new Date().getTime());
                this.Put("search_url",prefix_url + "," + JSON.stringify(post));
            }
            java.put("x", prefix_url + "," + JSON.stringify(post).replace("{" + "{key}}", key));
            java.put("key", key);
            return prefix_url + "," + JSON.stringify(post);
        }
    },
    buildUrl(selector, purl, extra) {
        extra = extra || {};
        sort_selector = extra.sort_selector || this.params.sort_selector;
        top_selector = extra.top_selector || this.params.top_selector;
        quanben_selector = extra.quanben_selector || this.params.quanben_selector;
        selector = extra.selector || selector;
        _selectors = extra.selectors || [];
        webview = extra.webview || "";
        if (typeof (selector) === "string") {
            url = extra.url || purl;
            html = webview ? java.ajax(url + ",{'webView': true}"):java.ajax(url);
            _selector = org.jsoup.Jsoup.parse(html);
            selector = String(selector) === "" ? _selector : _selector.select(selector);
        }
        if (typeof (selector) === "function") {
            sort_url = selector(sort_selector);
            top_url = selector(top_selector);
            quanben_url = selector(quanben_selector);
        } else {
            sort_url = selector.select(sort_selector + this.params.postfix_selector).attr("href");
            top_url = selector.select(top_selector + this.params.postfix_selector).attr("href");
            quanben_url = selector.select(quanben_selector + this.params.postfix_selector).attr("href");
        }
        sort_url = this.fixurl(sort_url, purl);
        java.log("分类：" + sort_url);
        top_url = this.fixurl(top_url, purl);
        java.log("排行：" + top_url);
        quanben_url = this.fixurl(quanben_url, purl);
        java.log("全本：" + quanben_url);
        urls = [sort_url, top_url, quanben_url];
        _selectors.map(w => {
            if (typeof (selector) === "function")
                t_url = selector(w);
            else
                t_url = selector.select(w + this.params.postfix_selector).attr("href");
            urls.push(fixurl(t_url, purl));
        });
        return urls;
    },
    buildExplore(selectors, extra) {
        extra = extra || {};
        list = extra.list || [];
        purl = extra.url || String(source.getKey());
        _selectors = extra.selectors || new Array(selectors.length);
        webview = extra.webview || "";
        selectors.map(([title, url, _selector], sindex) => {
            cate = [];
            if (typeof (_selector) === "function") {
                html = webview ? java.ajax(url + ",{'webView': true}"):java.ajax(url);
                list.push(this.explore(title, "", 1, 1, true));
                cate = _selector(html, sindex);
            } else if (_selector === "" && !new RegExp(purl.replace(/https?:/, "(^https?:)?") + "\/*$").test(url)) {
                list.push(this.explore(title, "", 1, 1, true));
                href = url;
                href = new RegExp(purl.replace(/https?:/, "(^https?:)?")).test(href) || /^\/\w+/.test(href) ? purl + String(href).replace(/(^https?:)?\/\/.*?\//, "/") : href;
                href = this.fixurl(href, purl);
                链接处理工具 = new UrlProcessor();
                链接处理工具 = 链接处理工具.链接(String(href));
                链接处理工具 = extra.rule ? 链接处理工具.插入新处理规则(extra.rule) : 链接处理工具;
                链接处理工具 = extra.page ? 链接处理工具.页码(1) : 链接处理工具;
                true_url = 链接处理工具.获取处理结果();
                cate.push(this.explore(title, true_url, -1, 0.25, true));
            } else if (_selector != "") {
                html = webview ? java.ajax(url + ",{'webView': true}"):java.ajax(url);
                list.push(this.explore(title, "", 1, 1, true));
                cate = org.jsoup.Jsoup.parse(html).select(_selector + this.params.postfix_selector).toArray();
                cate = cate.map((w, index) => {
                    if (Array.isArray(w)) {
                        title = w[0];
                        href = w[1];
                    } else {
                        title = String(w.text());
                        href = String(w.attr("href"));
                        if (/More|更多/.test(title)) {
                            title = String(w.parent().text()).replace("More+", "").replace(/\d+./, "").replace("更多", "");
                        }
                    }
                    href = new RegExp(purl.replace(/https?:/, "(^https?:)?")).test(href) || /^\/\w+/.test(href) ? purl + String(href).replace(/(^https?:)?\/\/.*?\//, "/") : href;
                    href = fixurl(href, purl);
                    链接处理工具 = new UrlProcessor();
                    链接处理工具 = 链接处理工具.链接(String(href)).索引(index);
                    链接处理工具 = extra.rule ? 链接处理工具.插入新处理规则(extra.rule) : 链接处理工具;
                    链接处理工具 = extra.page ? 链接处理工具.页码(1) : 链接处理工具;
                    true_url = 链接处理工具.获取处理结果();
                    return this.explore(title, true_url, 1, 0.25, false);
                });
            }
            if (_selectors[sindex] != null) {
                cate = (_selectors[sindex]).concat(cate);
            }
            list = list.concat(cate);
        });
        java.log(JSON.stringify(list));
        java.longToast("发现规则生成成功!到日志复制更新到发现url");
        return JSON.stringify(list);
    },
    buildId(top_url, regExp, index) {
        ruleExp = [];
        if (regExp.constructor === Array) {
            this.regexMap = regExp.concat(this.regexMap)
        } else {
            this.regexMap.unshift(regExp)
        }
        for (let i = 0; i < this.regexMap.length; i++) {
            let regObj = this.regexMap[i];
            let urlExp = regObj.reg;
            if (typeof urlExp === 'string') {
                urlExp = new RegExp(urlExp)
            }
            if (urlExp.test(top_url)) {
                ruleExp = regObj.exec(top_url);
                break;
            }
        }
        ruleExp = index ? ruleExp[index] : ruleExp;
        return ruleExp;
    },
    buildCombinsExplore(top_url, selectors, extra) {
        extra = extra || {};
        rule = extra.rule || [];
        list = extra.list || [];
        toUrl = extra.toUrl || this.toUrl;
        webview = extra.webview || "";
        _selectors = extra.selectors || new Array(selectors.length);
        url = source.getKey();
        purl = String(url);
        let ruleExp = this.buildId(top_url, rule);
        java.log(String(ruleExp));
        html = webview ? java.ajax(top_url + ",{'webView': true}"):java.ajax(top_url);
        selector = org.jsoup.Jsoup.parse(html);
        selectorIndexs = [];
        ruleExpIndex = ruleExp.length - 1;
        sindex = selectors.length;
        java.log(sindex);
        java.log(ruleExpIndex);
        selectors = selectors.map(([_selector, _selectorIndex], index) => {
            cate = [];
            urlIndex = sindex + _selectorIndex - ruleExpIndex;
            urlIndex = urlIndex >= sindex ? 0 : urlIndex;
            selectorIndexs.push(urlIndex);
            if (typeof (_selector) === "function") {
                cate = _selector(html, _selectorIndex);

            } else if (typeof (_selector) === "string") {
                cate = org.jsoup.Jsoup.parse(html).select(_selector + this.params.postfix_selector).toArray();
                cate = cate.map((w, index) => {
                    title = w.text();
                    href = this.buildId(w.attr("href"), rule, _selectorIndex);
                    return [title, href];
                });

                java.log(index);
                if (_selectors[index] != null) {
                    cate = (_selectors[index]).concat(cate);
                }
                java.log(JSON.stringify(cate));
            }
            return cate;
        });
        java.log(JSON.stringify(selectorIndexs));
        combinsSelectors = this.combins(selectors);
        i = 0; while (combinsSelector = combinsSelectors.next()) {
            if (i % selectors[0].length == 0) {
                sliceCombinsSelector = combinsSelector.slice(1); title = sliceCombinsSelector.map(([name, url]) => {
                    return name;
                });
                list.push(this.explore(title.join("-"), "", 1, 1, true));
            }
            titles = combinsSelector.map(([name, url]) => {
                return name;
            });

            urls = selectorIndexs.map(urlIndex => {
                return combinsSelector[urlIndex][1];
            });
            list.push(this.explore(titles.join("-"), this.toUrl(purl, urls, ruleExp), 1, 0.3, false));
            i++;
        }
        java.log(JSON.stringify(list));
        java.longToast("发现规则生成成功!到日志复制更新到发现url");
        return JSON.stringify(list);
    },
    toUrl(purl, urls, ruleExp) {
        return purl + ruleExp[1] + urls.join("") + ruleExp[ruleExp.length - 1].replace(/1.html$/, "{" + "{page}}.html");
    },
    explore(title, url, Grow, Basis, bool) {
        style = {
            title: title,
            url: url,
            style: {
                layout_flexGrow: Grow,
                layout_flexBasisPercent: Basis,
                layout_wrapBefore: bool
            }
        };
        return style;
    },
    checkParams() {
        if (!this.params.html) {
            throw new Error("请先调用 .baseHtml(html) 传入源码进行初始化!")
        }
    },
    baseHtml(html) {
        this.params.html = html
        return this
    },
    addValidateExtra(regExp) {
        if (regExp.constructor === Array) {
            this.validateRegexMap = regExp.concat(this.validateRegexMap)
        } else {
            this.validateRegexMap.unshift(regExp)
        }
        return this;
    },
    addExtra(regExp) {
        if (regExp.constructor === Array) {
            this.regexMap = regExp.concat(this.regexMap)
        } else {
            this.regexMap.unshift(regExp)
        }
        return this;
    },
    debug() {
        this.params.debug = true
        return this;
    },
    validate() {
        this.checkParams()
        let html = ''
        for (let i = 0; i < this.validateRegexMap.length; i++) {
            let regObj = this.validateRegexMap[i];
            let urlExp = regObj.reg;
            if (typeof urlExp === 'string') {
                urlExp = new RegExp(urlExp)
            }
            if (urlExp.test(this.params.html)) {
                if (this.params.debug) {
                    java.log(urlExp.toString())
                }
                html = regObj.exec(this);
                break;
            }
        }
        if (html) {
            return html;
        } else {
            return this.params.html;
        }
    },
    buildMapUrl(key, extra) {
        extra = extra || {};
        key = extra.key || this.key;
        page = extra.page || this.page;
        url = extra.url || String(source.getKey()).replace(/#.*/, "");
        map_selector = extra.map_selector || "a:matches((网站地图|地图导航|全部小说))";
        webview = extra.webview || '';
        html = webview ? java.ajax(url + ",{'webView': true}"):java.ajax(url);
        java.put("key", key);
        java.put("page", page);
        quanbu = org.jsoup.Jsoup.parse(html).select(map_selector).attr("href");
        quanbu = quanbu != "" && !quanbu.endsWith(".xml") ? quanbu : org.jsoup.Jsoup.parse(html).select("a:matches((小说大全))").attr("href");
        return quanbu;
    },
    buildList(html, extra) {
        extra = extra || {};
        url = extra.url || this.baseUrl;
        key = extra.key || java.get("key");
        baseUrl = extra.baseUrl || this.baseUrl;
        page = extra.page || java.get("page");
        page = page || 1;
        let list = [];
        let htmls = [];
        try {
            _lists = extra.lists || String("a:contains(" + key + ")"+this.params.postfix_selector);
            _urls = extra.urls || ".layui-table@tag.a@href";
            purl = String(url).match(/^(https|http|ftp)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,}/)[0];
            time = this.Get("time") || new Date().getTime();
            if (this.Get("urls") && new Date().getTime() - time < 580501424) {
                urls = this.Get("urls") || [];
            } else {
                if (typeof (_urls) === "function") {
                    urls = _urls(html);
                } else if (_urls === ".layui-table@tag.a@href") {
                    urls = [];
                    pageUrls = java.getStringList(".layui-table@tag.a@href", html, true).toArray();
                    for (let url of pageUrls) {
                        html = java.ajax(url);
                        urls = urls.concat(java.getStringList("#update_page@tag.a@href", html, true).toArray());
                    }
                    if (urls.length == 0) {
                        urls = urls.concat(pageUrls);
                    }
                } else if (typeof (_urls) === "string") {
                    urls = java.getStringList(_urls, html, true);
                    urls = urls.toArray().map(w => {
                        href = String(w);
                        href = this.fixurl(href, purl);
                        return href;
                    });
                }
                if (java.getElements("id.page").length > 0) {
                    n = 10;
                    content = java.ajax(java.getString("id.page@tag.a.-2@href", html, true));
                    while (java.getString("id.page@tag.a.-1@href", content, false) != "") {
                        java.log(java.getString("id.page@tag.a.-2@href", content, true));
                        n = String(java.getString("id.page@tag.a.-2@href", content, true)).match(/\/(\d+).html$/)[1];
                        content = java.ajax(java.getString("id.page@tag.a.-2@href", content, true));
                    }
                    for (var i = 1; i < n; i++) {
                        urls.push(String(baseUrl).replace(/\d+.html/, i + ".html"));
                    }
                }
                if (Array.isArray(urls) && urls.length > 0) {
                    this.Put("time", new Date().getTime());
                    this.Put("urls", urls);
                }
            }
            java.log(JSON.stringify(urls));
            if (urls && urls.length > 0) {
                for (let i = page - 1; i < Math.ceil(urls.length / 20); i++) {
                    slice_url = urls.slice(i * 20, (i + 1) * 20);
                    responses = java.ajaxAll(slice_url);
                    htmls = responses.map(w => w.body());
                    data = htmls.join("\n");
                    java.setContent(data);
                    if (typeof (_lists) === "function") {
                        list = list.concat(_lists(data));
                    } else if (typeof (_lists) === "string") {
                        java.setContent(data);
                        list = list.concat(java.getElements(_lists));
                    }
                    java.log(i + " : " + slice_url[slice_url.length - 1] + " key: " + key);
                    if (list.length >= 100) {
                        break;
                    }
                    if (list.length > 0 && i > page + 5) {
                        break;
                    }
                }
            } else {
                if (typeof (_lists) === "function") {
                    list = list.concat(_lists(html));
                } else if (typeof (_lists) === "string") {
                    java.setContent(html);
                    list = list.concat(java.getElements(_lists));
                }
            }
        } catch (err) {
            return list;
        }
        return list;
    },
    Get(e) {
        var Variable = String(source.getVariable());
        if(/^\s*{/.test(Variable)) {
            get = JSON.parse(Variable);
        } else {
            source.setVariable("");
            get = {};
        }
        return get[e];
    },
    Put(e, data) {
        var Variable = String(source.getVariable());
        if(/^\S*{/.test(Variable)) {
            get = JSON.parse(Variable);
        } else {
            source.setVariable("");
            get = {};
        }
        get[e] = data;
        source.setVariable(JSON.stringify(get, null, '\t'));
        return get;
    }
})
/**
 * 笛卡尔积
 * @returns 多维数组
 */
function combins() {
    if (arguments.length < 2 && !Array.isArray(arguments[0][0]))
        return arguments[0] || [];
    var args = arguments.length == 1 ? Array.prototype.slice.call(arguments[0]) : Array.prototype.slice.call(arguments);
    return new legadoProcessor(this).combins(arguments);
}

/**
 * 
 * @param {*} selector 
 * @param {*} selectors 
 * @param {*} selector_indexs 
 * @param {*} re 
 * @param {*} list 
 * @param {*} toUrl 
 * @param {*} postfix 
 * @param {*} extra 
 * @returns 
 */
function build(selector, selectors, selector_indexs, re, list, toUrl, postfix, extra) {
    const { java, source, cookie, cache } = this;
    params = postfix.match(re);
    java.log(String(params));
    prefix = params[1];
    postfix = params[params.length - 1];
    names = selectors.map(w => {
        return selector.select(w).toArray().map(t => t.text());
    });
    if (extra) {
        names = names.map((w, index) => { w = w.concat(extra["name"][index]); return w; });
        names = names.concat(extra["name"].slice(names.length));
    }
    java.log(JSON.stringify(names));
    urls = selectors.map((w, index) => {
        return selector.select(w).toArray().map(t => t.attr("href").match(re)[selector_indexs[index]]);
    });
    if (extra) {
        urls = urls.map((w, index) => { w = w.concat(extra["url"][index]); return w; });
        urls = urls.concat(extra["url"].slice(urls.length));
    }
    java.log(JSON.stringify(urls));
    name4 = combins(names);
    java.log(JSON.stringify(name4));
    url4 = combins(urls);
    var i = 0;
    while (name5 = name4.next()) {
        url5 = url4.next();
        if (i % names[0].length == 0) {
            name6 = name5.slice(1);
            list.push({
                "title": name6.join("-"),
                "url": "",
                "style": {
                    "layout_flexBasisPercent": 1,
                    "layout_flexGrow": 1
                }
            });
        }
        item = {
            "title": name5.join("-"),
            "url": toUrl(url5, prefix, postfix),
            "style": {
                "layout_flexBasisPercent": 0.4
            }
        }
        list.push(item);
        i++;
    }
    java.log(JSON.stringify(list));
    return list;
}

function legadoPost(re, extra) {
    return new legadoProcessor(this).request("POST", re, extra);
}

function legadoGet(re, extra) {
    return new legadoProcessor(this).request("GET", re, extra);
}

function UrlProcessor() {
    this.params = {}
    this.regexMap = [
        {
            reg: /\w*\d+[\/_-]1(\/?(.s?html?)?)$/,
            exec: () => {
                if (this.params.page) {
                    return this.params.url.replace(/1(\/?(.s?html?)?)$/, "{" + "{page}}$1");
                } else {
                    return this.params.url.replace(/1(\/?(.s?html?)?)$/, "1$1");
                }
            }
        },
        {
            reg: /\w+[\/_-]1(\/|(.s?html?))$/,
            exec: () => {
                if (this.params.page) {
                    return this.params.url.replace(/1(\/|(.s?html?))$/, "{" + "{page}}$1");
                } else {
                    return this.params.url.replace(/(\/?(.s?html?)?)$/, "$1");
                }
            }
        },
        {
            reg: /(top|Top|paihang)[\/_-]*\w+[\/_-]1(\/?(.s?html?)?)$/,
            exec: () => {
                if (this.params.page) {
                    return this.params.url.replace(/1(\/?(.s?html?)?)$/, "{" + "{page}}$1");
                } else {
                    return this.params.url.replace(/(\/?(.s?html?)?)$/, "$1");
                }
            }
        },
        {
            reg: /(top|Top)\/*\w+(\/|(.s?html?))$/,
            exec: () => {
                if (this.params.page) {
                    return this.params.url.replace(/(\/|(.s?html?))$/, "/{" + "{page}}.html");
                } else {
                    return this.params.url.replace(/(\/?(.s?html?)?)$/, "$1");
                }
            }
        },
        {
            reg: /(full|wanben|quanben)\/1(\/|(.s?html?))$/,
            exec: () => {
                if (this.params.page) {
                    return this.params.url.replace(/\d*(\/?(.s?html?)?)$/, "{" + "{page}}$1");
                } else {
                    return this.params.url.replace(/(\/?(.s?html?)?)$/, "$1");
                }
            }
        },
        {
            reg: /(full|wanben|quanben)(\/?(.s?html?)?)$/,
            exec: () => {
                if (this.params.page) {
                    return this.params.url.replace(/\d*(\/?(.s?html?)?)$/, "/{" + "{page}}");
                } else {
                    return this.params.url.replace(/(\/?(.s?html?)?)$/, "$1");
                }
            }
        },
        {
            reg: /\/(paihangbang|paihang|rank|tuijian|collect|shoucang|top|Top|over|ph|wanbenxiaoshuo)(\/|(.s?html?))$/,
            exec: () => {
                return this.params.url.replace(/(\/?(.s?html?)?)$/, "$1");
            }
        },
        {
            reg: /(\/|(.s?html?))$/,
            exec: () => {
                if (this.params.page) {
                    return this.params.url.replace(/(\/|(.s?html?))$/, "/{" + "{page}}.html");
                } else {
                    return this.params.url.replace(/(\/?(.s?html?)?)$/, "$1");
                }
            }
        }
    ]
}

Object.assign(UrlProcessor.prototype, {
    constructor: UrlProcessor,
    checkParams() {
        if (!this.params.url) {
            throw new Error("请先调用 .baseUrl(url) 传入链接进行初始化!")
        }
    },
    baseUrl(url) {
        this.params.url = url
        return this
    },
    page(page) {
        this.params.page = page
        return this
    },
    index(index) {
        this.params.index = index
        return this
    },
    addExtra(regExp) {
        if (regExp.constructor === Array) {
            this.regexMap = regExp.concat(this.regexMap)
        } else {
            this.regexMap.unshift(regExp)
        }
        return this;
    },
    debug() {
        this.params.debug = true
        return this;
    },
    exec() {
        this.checkParams()
        let true_url = ''
        for (let i = 0; i < this.regexMap.length; i++) {
            let regObj = this.regexMap[i];
            let urlExp = regObj.reg;
            if (typeof urlExp === 'string') {
                urlExp = new RegExp(urlExp)
            }
            if (urlExp.test(this.params.url)) {
                if (this.params.debug) {
                    this.java.log(urlExp.toString())
                }
                true_url = regObj.exec(this);
                break;
            }
        }
        if (true_url) {
            return true_url;
        } else {
            return this.params.url
        }
    },
    获取处理结果() {
        return this.exec()
    },
    链接(url) {
        return this.baseUrl(url)
    },
    页码(page) {
        return this.page(page)
    },
    索引(index) {
        return this.index(index)
    },
    插入新处理规则(regExp) {
        return this.addExtra(regExp)
    },
    legado(java, source, cookie) {
        this.java = java
        this.source = source
        this.cookie = cookie
        return this
    },
    调试模式() {
        return this.debug();
    }
});

function buildUrl(selector, purl, extra) {
    return new legadoProcessor(this).buildUrl(selector, purl, extra);
}

function fixurl(url, purl) {
    url = /^https?/.test(url) ? String(url).replace(/(^https?:)?\/\/.*?\//, "/") : url;
    url = /^\//.test(url) ? url : "/" + url;
    url = purl + String(url);
    return url;
}
function buildA(purl, toFind, selectors, extra) {
    extra = extra || {};
    const { java, source, cookie, cache } = this;
    list = [];
    let cate;
    for (let selector of selectors) {
        if (selector[2] === "" && selector[1] != purl) {
            list.push(toFind(selector));
        } else if (selector[2] !== "") {
            html = java.ajax(selector[1]);
            _selector = selector[2];
            if (typeof (_selector) === "function") {
                cate = _selector(html)
            } else if (typeof (_selector) === "string") {
                cate = Jsoup.parse(html).select(selector[2] + ":not([href~=javascript]):not([href=/]):not([href^=#])").toArray().map(toFind);
            } else {
                cate = []
            }
            if (cate.length > 0) {
                list.push({
                    "title": selector[0],
                    "url": "",
                    "style": {
                        "layout_flexBasisPercent": 1,
                        "layout_flexGrow": 1
                    }
                });
            } else if (selector[1] != purl) {
                list.push(toFind(selector));
            }
            list = list.concat(cate);
        }
    }
    return list;
}

function buildMapUrl(key, extra) {
    return new legadoProcessor(this).buildMapUrl(key, extra);
}

function _urls(html, extra) {
    extra = extra || {};
    const { java, source, cookie, cache } = this;
    baseUrl = extra.baseUrl || this.baseUrl;
    url_selector = extra.url_selector || ".layui-table@tag.a@href";
    page_selector = extra.page_selector || "#update_page@tag.a@href";
    list = [];
    try {
        urls = java.getStringList(url_selector, result, true);
        for (let url of urls.toArray()) {
            html = java.ajax(url);
            list = list.concat(java.getStringList(page_selector, html, true).toArray());
        }
        if (list.length > 0) {
            list = list.concat(urls);
        }
        else {
            list.push(baseUrl);
        }
    } catch (err) {
        return list;
    }
    return list;
}

function buildList(html, extra) {
    return new legadoProcessor(this).buildList(html, extra)
}

function base64(_str) {
    var staticchars = "PXhw7UT1B0a9kQDKZsjIASmOezxYG4CHo5Jyfg2b8FLpEvRr3WtVnlqMidu6cN";
    var encodechars = "";
    for (var i = 0; i < _str.length; i++) {
        var num0 = staticchars.indexOf(_str[i]);
        if (num0 == -1) {
            var code = _str[i]
        } else {
            var code = staticchars[(num0 + 3) % 62]
        }
        var num1 = parseInt(Math.random() * 62, 10);
        var num2 = parseInt(Math.random() * 62, 10);
        encodechars += staticchars[num1] + code + staticchars[num2]
    }
    return encodechars
}

function buildBooks(books, extra) {
    extra = extra || {};
    const { java, source, cookie, cache } = this;
    Object.keys(source)
    for (let key of Object.keys(source)) {
        if (typeof (source[key]) !== "function") {
            if (typeof (books[key]) === "undefined" && !key.startsWith("rule") && typeof (source[key]) !== "undefined" && key !== "source" && !key.endsWith("Rule") && !key.startsWith("variable") && !key.endsWith("Map") && key !== "shareScope" && key !== "invalidGroupNames" && key !== "loginInfo" && key !== "key" && key !== "disPlayNameGroup" && key !== "stability" && key !== "class" && key !== "loginHeader" && key !== "tag" && key !== "loginJs" && source[key] != null && source[key] != "") {
                books[key] = source[key];
            } else if (key.startsWith("rule") && key !== "ruleReview") {
                books[key] = {};
                for (let rule in source[key]) {
                    if (typeof (source[key][rule]) !== "function" && rule !== "stability" && rule !== "class" && rule !== "init" && rule !== "updateTime" && rule !== "vip" && rule !== "pay" && rule !== "volume" && source[key][rule] != null && source[key][rule] != "") {
                        books[key][rule] = source[key][rule] ? source[key][rule] : "";
                    }
                }
            }
        }
    }
    return books;
}

function validateCode(html, extra) {
    extra = extra || {};
    阅读处理工具 = new legadoProcessor(this);
    阅读处理工具 = extra.rule ? 阅读处理工具.插入新处理规则(extra.rule) : 阅读处理工具;
    return 阅读处理工具.baseHtml(html).validate();
}

function explore(title, url, Grow, Basis, bool) {
    return new legadoProcessor(this).explore(title, url, Grow, Basis, bool);
}

function Map(e) {
    const { java, source, cookie, cache } = this;
    var infomap = source.getLoginInfoMap();
    var map = (infomap !== null && infomap.get(e) && String(infomap.get(e)).length > 0) ? infomap.get(e) : '';
    return String(map);
}
function sleep(milliseconds) {
    var start = new Date().getTime();
    while (new Date().getTime() - start < milliseconds) {
    }
}
function Checkwait(e) {
    while (e == '') {
        sleep(10000);
        return true;
    }
    return e;
}

function buildExplore(selectors, extra) {
    return new legadoProcessor(this).buildExplore(selectors, extra);
}

function buildCombinsExplore(top_url, selectors, extra) {
    return new legadoProcessor(this).buildCombinsExplore(top_url, selectors, extra);
}

function toUrl(urls, ruleExp) {
    return purl + ruleExp[1] + urls.join("") + ruleExp[ruleExp.length - 1].replace(/1.html$/, "{" + "{page}}.html");
}