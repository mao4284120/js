import requests
import pandas as pd
from lxml import etree
from zlib import decompress  # 解压

df = pd.DataFrame()
for i in range(1, 23):
    url = f'https://cmts.iqiyi.com/bullet/64/00/1078946400_300_{i}.z'
    bulletold = requests.get(url).content  # 得到二进制数据
    decode = decompress(bulletold).decode('utf-8')  # 解压解码
    with open(f'{i}.html', 'a+', encoding='utf-8') as f:  # 保存为静态的html文件
        f.write(decode)

    html = open(f'./{i}.html', 'rb').read()  # 读取html文件
    html = etree.HTML(html)  # 用xpath语法进行解析网页
    ul = html.xpath('/html/body/danmu/data/entry/list/bulletinfo')
    for i in ul:
        contentid = ''.join(i.xpath('./contentid/text()'))
        content = ''.join(i.xpath('./content/text()'))
        likeCount = ''.join(i.xpath('./likecount/text()'))
        print(contentid, content, likeCount)
        text = pd.DataFrame({'contentid': [contentid], 'content': [content], 'likeCount': [likeCount]})
        df = pd.concat([df, text])
df.to_csv('哥斯拉大战金刚.csv', encoding='utf-8', index=False)
